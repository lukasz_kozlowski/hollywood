/******************************************************************************
   Copyright 2020 Łukasz Kozłowski

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/

#include <chrono>
#include <iostream>
#include "actorsystem.hpp"
#include "actorwithtimers.hpp"
#include "receiver.hpp"

using namespace hollywood;

class SimpleActor : public Actor<SimpleActor>, public Receiver<int> {
public:
    void receive(const int& m) { std::cout << "SimpleActor receive: " << m << std::endl; }
};

struct Message {
    int num;
};

class Actor1 : public Actor<Actor1>, public Receiver<Message> {
public:
    void receive(const Message& message) { std::cout << "Actor1: receive Message " << message.num << std::endl; }
};

class ActorWitMultipleMessages : public Actor<ActorWitMultipleMessages>, public Receiver<int, std::string> {
public:
    void receive(const int& message) { std::cout << "ActorWitMultipleMessages: receive int " << message << std::endl; }

    void receive(const std::string& message) {
        std::cout << "ActorWitMultipleMessages: receive string " << message << std::endl;
    }
};

class ActorWithTimer : public ActorWithTimers<ActorWithTimer>, public Receiver<Message> {
public:
    ActorWithTimer() : message_{1} {
        getSelf().tell(message_);
        startSingleTimer(message_, std::chrono::seconds(5));
        startTimerWithFixedDelay(message_, std::chrono::seconds(1));
    }

    void receive(const Message& message) {
        ++handledMessagesCount_;
        std::cout << "ActorWithTimer: Handling message " << handledMessagesCount_ << std::endl;
    }

private:
    Message message_;
    int handledMessagesCount_ = 0;
};

int main() {
    auto actorSystem = ActorSystem::create("Test");

    auto simpleActor = actorSystem->actorOf<SimpleActor>();
    simpleActor->tell(123);

    Message message{1234};
    auto actor1 = actorSystem->actorOf<Actor1>();
    actor1->tell(message);

    auto actorWithMultipleMessages = actorSystem->actorOf<ActorWitMultipleMessages>();
    actorWithMultipleMessages->tell(42);
    actorWithMultipleMessages->tell("test");

    auto actorWithTimer = actorSystem->actorOf<ActorWithTimer>();

    actorSystem->waitForFinish();
    return 0;
}
