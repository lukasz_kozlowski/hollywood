/******************************************************************************
   Copyright 2020 Łukasz Kozłowski

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/

#ifndef HOLLYWOOD_ACTOR_HPP
#define HOLLYWOOD_ACTOR_HPP

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <boost/asio.hpp>
#include <memory>
#include "actorref.hpp"

namespace hollywood {

class ActorSystem;

template <class ActorImplementation>
class Actor {
public:
    using ActorClass = ActorImplementation;

    Actor() { ; }
    virtual ~Actor() { ; }

    void setActorSystem(ActorSystem* actorSystem) { actorSystem_ = actorSystem; }

    void createSelfReference(boost::asio::io_context& ioContext) {
        self_ = new ActorRef<ActorClass>(ioContext, static_cast<ActorClass*>(this));
    }

protected:
    const ActorRef<ActorClass>& getSelf() const { return *self_; }

    ActorSystem& getSystem() const { return *actorSystem_; }

private:
    ActorRef<ActorClass>* self_;
    ActorSystem* actorSystem_;
};

}  // namespace hollywood

#endif  // HOLLYWOOD_ACTOR_HPP
