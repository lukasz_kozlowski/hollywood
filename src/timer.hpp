/******************************************************************************
   Copyright 2020 Łukasz Kozłowski

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/

#ifndef HOLLYWOOD_TIMER_HPP
#define HOLLYWOOD_TIMER_HPP

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include "actorref.hpp"

namespace hollywood {

template <class ActorClass, typename MessageType>
class Timer {
public:
    using TimerType = boost::asio::steady_timer;
    using DurationType = TimerType::duration;

    Timer(boost::asio::io_context& ioContext,
          DurationType duration,
          MessageType message,
          const ActorRef<ActorClass>& messageReceiver)
        : message_(message), messageReceiver_(messageReceiver), duration_(duration) {
        timer_ = std::make_unique<TimerType>(ioContext, duration);
    }

    void start() { timer_->async_wait(boost::bind(&Timer::performTimerTick, this)); }

protected:
    MessageType message_;
    const ActorRef<ActorClass>& messageReceiver_;
    DurationType duration_;
    std::unique_ptr<TimerType> timer_;

    virtual void performTimerTick() = 0;
};

}  // namespace hollywood

#endif  // HOLLYWOOD_TIMER_HPP
