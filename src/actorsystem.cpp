/******************************************************************************
   Copyright 2020 Łukasz Kozłowski

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/

#include "actorsystem.hpp"

namespace hollywood {

ActorSystem::ActorSystem(const std::string& name) : name_(name), workGuard_(boost::asio::make_work_guard(ioContext_)) {
    ;
}

std::unique_ptr<ActorSystem> ActorSystem::create(const std::string& name) {
    auto actorSystem = std::make_unique<ActorSystem>(name);
    actorSystem->run();
    return actorSystem;
}

const std::string& ActorSystem::getName() const {
    return name_;
}

void ActorSystem::run() {
    auto workerThread = std::make_unique<std::thread>([&]() { ioContext_.run(); });
    workerThreads_.push_back(std::move(workerThread));
}

void ActorSystem::waitForFinish() {
    for (const auto& workerThread : workerThreads_) {
        workerThread->join();
    }
}

boost::asio::io_context& ActorSystem::getIoContext() {
    return ioContext_;
}

}  // namespace hollywood
