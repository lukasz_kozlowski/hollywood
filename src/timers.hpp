/******************************************************************************
   Copyright 2020 Łukasz Kozłowski

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/

#ifndef HOLLYWOOD_TIMERS_HPP
#define HOLLYWOOD_TIMERS_HPP

#include "singletimer.hpp"
#include "timer.hpp"
#include "timerwithfixeddelay.hpp"

namespace hollywood {

/**
 * Mix-in class to provide timer funcionality to Actor-like classes.
 */
template <class ActorClass>
class Timers : public ActorClass {
protected:
    template <typename MessageType>
    void startSingleTimer(MessageType message, typename Timer<ActorClass, MessageType>::DurationType duration) {
        using TimerType = SingleTimer<typename ActorClass::ActorClass, MessageType>;
        auto timer_ = new TimerType(this->getSystem().getIoContext(), duration, message, this->getSelf());
        timer_->start();
    }

    template <typename MessageType>
    void startTimerWithFixedDelay(MessageType message, typename Timer<ActorClass, MessageType>::DurationType duration) {
        using TimerType = TimerWithFixedDelay<typename ActorClass::ActorClass, MessageType>;
        auto timer_ = new TimerType(this->getSystem().getIoContext(), duration, message, this->getSelf());
        timer_->start();
    }
};

}  // namespace hollywood

#endif  // HOLLYWOOD_TIMERS_HPP
