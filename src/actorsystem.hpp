/******************************************************************************
   Copyright 2020 Łukasz Kozłowski

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/

#ifndef HOLLYWOOD_ACTORSYSTEM_HPP
#define HOLLYWOOD_ACTORSYSTEM_HPP

#include <list>
#include <memory>
#include <string>
#include <thread>
#include "actor.hpp"
#include "actorref.hpp"

namespace hollywood {

class ActorSystem {
public:
    ActorSystem(const std::string& name);

    static std::unique_ptr<ActorSystem> create(const std::string& name);

    const std::string& getName() const;
    void run();
    void waitForFinish();

    /**
     * Create new actor.
     *
     * @param arguments Arguments that should be passed to Actor constructor.
     */
    template <class ActorClass, typename... ActorConstructorArguments>
    std::unique_ptr<ActorRef<ActorClass>> actorOf(ActorConstructorArguments... arguments) {
        std::allocator<ActorClass> actorsAllocator;
        ActorClass* actorAllocatedMemory = actorsAllocator.allocate(1);
        actorAllocatedMemory->setActorSystem(this);
        actorAllocatedMemory->createSelfReference(ioContext_);
        ActorClass* actor = new (actorAllocatedMemory) ActorClass(arguments...);
        return std::make_unique<ActorRef<ActorClass>>(ioContext_, actor);
    }

    boost::asio::io_context& getIoContext();

    template <class Duration>
    boost::asio::steady_timer createTimer(Duration& duration) {
        return boost::asio::steady_timer(ioContext_, duration);
    }

private:
    std::string name_;
    boost::asio::io_context ioContext_;
    decltype(boost::asio::make_work_guard(ioContext_)) workGuard_;
    std::list<std::unique_ptr<std::thread>> workerThreads_;
};

}  // namespace hollywood

#endif  // HOLLYWOOD_ACTORSYSTEM_HPP
