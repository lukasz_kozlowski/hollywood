/******************************************************************************
   Copyright 2020 Łukasz Kozłowski

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/

#ifndef HOLLYWOOD_ACTORREF_HPP
#define HOLLYWOOD_ACTORREF_HPP

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <boost/asio.hpp>

namespace hollywood {

template <class ActorClass>
class ActorRef {
public:
    ActorRef(boost::asio::io_context& ioContext, ActorClass* actor) : ioContext_(ioContext), actor_(actor) {}

    template <class MessageType>
    void tell(MessageType message) const {
        ioContext_.post([=]() { actor_->receive(message); });
    }

private:
    boost::asio::io_context& ioContext_;
    ActorClass* actor_;
};

}  // namespace hollywood

#endif  // HOLLYWOOD_ACTORREF_HPP
