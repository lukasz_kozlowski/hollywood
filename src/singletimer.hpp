/******************************************************************************
   Copyright 2020 Łukasz Kozłowski

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
******************************************************************************/

#ifndef HOLLYWOOD_SINGLETIMER_HPP
#define HOLLYWOOD_SINGLETIMER_HPP

#include "timer.hpp"

namespace hollywood {

template <class ActorClass, typename MessageType>
class SingleTimer : public Timer<ActorClass, MessageType> {
public:
    using DurationType = typename Timer<ActorClass, MessageType>::DurationType;

    SingleTimer(boost::asio::io_context& ioContext,
                DurationType duration,
                MessageType message,
                const ActorRef<ActorClass>& messageReceiver)
        : Timer<ActorClass, MessageType>(ioContext, duration, message, messageReceiver) {
        ;
    }

protected:
    void performTimerTick() override { this->messageReceiver_.tell(this->message_); }
};

}  // namespace hollywood

#endif  // HOLLYWOOD_SINGLETIMER_HPP
